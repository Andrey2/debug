<?php
function dmp($ar)
{
    echo('<br>');
    print_r(debug_backtrace(2)[0]);
    echo("<pre>");
    print_r($ar);
    echo("</pre>");
}

class MyLog
{
    var $fileName;
    function __construct($fileName)
    {
        $this->fileName=$fileName;
    }
    function log($ar,$capt)
    {
        $f = fopen($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/".$this->fileName, "a+");
        fwrite($f, print_r($ar,true) . "\n===".$capt."==".date("d-m-Y H:i:s")."===\n");
        fclose($f);
    }
}

function debug()
{
    $ar=debug_backtrace(2);
    $out=[];
    foreach ($ar as $key=>$val)
    {

        if(strpos($val['file'],'/modules/')===false)
        {
            $out[$key]=$val;
        }

    }
    dmp($out);
}